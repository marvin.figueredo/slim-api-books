<?php
require 'vendor/autoload.php';

$app = new \Slim\Slim();

define("SPECIALCONSTANT", true);
require 'app/controllers/info.php';
require 'app/controllers/books.php';
require 'app/services/serviceBooks.php';
require 'app/libs/connect.php';

$app->run();