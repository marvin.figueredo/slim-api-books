<?php if(!defined("SPECIALCONSTANT")) die("Acceso denegado");
 
function getConnection()
{
    try{
        $db_username = "postgres";
        $db_password = "admin12345";
        $connection = new PDO("pgsql:host=localhost;dbname=my-api-test-db", $db_username, $db_password);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
    return $connection;
}