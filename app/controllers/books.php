<?php
use \App\Service\ServiceBooks;
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->get("/books/", function() use($app)
{
    try{
        $service = new ServiceBooks();
        $books = $service->ListAllBooks();
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($books));
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
});

$app->get('/book/:nro', function ($nro) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("SELECT * FROM books WHERE id = ?");
        $dbh->bindParam(1, $nro);
        $dbh->execute();
        $book = $dbh->fetch();
        $connection = null;
 
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($book));
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
});