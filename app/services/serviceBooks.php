<?php
namespace App\Service;
//require '../app/libs/connect.php';

class ServiceBooks{
    public function ListAllBooks(){
        try{
            $connection = getConnection();
            $dbh = $connection->prepare("SELECT * FROM books");
            $dbh->execute();
            $books = $dbh->fetchAll();
            $connection = null;
    
            return $books;
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
        
    }
}
